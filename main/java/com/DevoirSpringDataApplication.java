package com;

import java.util.Date;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.exercice1.entity.Client;
import com.exercice1.entity.Commande;
import com.exercice1.repository.ClientRepository;
import com.exercice1.repository.CommandeRepository;
import com.exercice2.entity.Etudiant;
import com.exercice2.entity.Evaluer;
import com.exercice2.entity.Matiere;
import com.exercice2.repository.EtudiantRepository;
import com.exercice2.repository.EvaluerRepository;
import com.exercice2.repository.MatiereRepository;

@SpringBootApplication
public class DevoirSpringDataApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(DevoirSpringDataApplication.class, args);
//		Exercice1
	    System.out.println("----------------------------------------------------------------------");
	    System.out.println("Exercice1");
	    System.out.println("----------------------------------------------------------------------");
//		Ajouter des clients
		Client c1 = new Client((long) 1, "Steev", "Johnson", 23);
		Client c2 = new Client((long) 2, "Donald", "trump", 30);
		Client c3 = new Client((long) 3, "Barak", "Obama", 35);
		Client c4 = new Client((long) 4, "Justin", "trudeau", 40);
		Client c5 = new Client((long) 5, "Vladimir", "Poutine", 45);
		Client c6 = new Client((long) 6, "Vladimir", "Johnson", 50);
		ClientRepository clientrep = (ClientRepository) context.getBean(ClientRepository.class);
	    System.out.println("----------------------------------------------------------------------");
	    System.out.println("Ajouter des clients");
	    System.out.println("----------------------------------------------------------------------");
		clientrep.save(c1);
		clientrep.save(c2);
		clientrep.save(c3);
		clientrep.save(c4);
		clientrep.save(c5);
		clientrep.save(c6);
//		Une méthode permettant de trouver une liste des clients en fonction d’un nom. 
	    System.out.println("----------------------------------------------------------------------");
	    System.out.println("Une méthode permettant de trouver une liste des clients en fonction d’un nom. ");
	    System.out.println("----------------------------------------------------------------------");
		System.out.println(clientrep.findByNom("Johnson"));

//      - Une méthode permettant de retourner la liste des clients dont l’âge est plus grand que le paramètre
//		passé
	    System.out.println("----------------------------------------------------------------------");
	    System.out.println("- Une méthode permettant de retourner la liste des clients dont l’âge est plus grand que le paramètre\n" + 
	    		"passé");
	    System.out.println("----------------------------------------------------------------------");
	    System.out.println(clientrep.findByAgeGreaterThan(35));
	    
//		Ajouter des commandes
	    System.out.println("----------------------------------------------------------------------");
	    System.out.println("Ajouter des commandes");
	    System.out.println("----------------------------------------------------------------------");
	    
	    Commande cm1 = new Commande((long) 1, "Vélo", c2);
	    Commande cm2 = new Commande((long) 2, "Voiture", c3);
	    Commande cm3 = new Commande((long) 3, "Moto", c4);
	    Commande cm4 = new Commande((long) 4, "Avion", c5);
	    Commande cm5 = new Commande((long) 5, "Tank", c5);
	    Commande cm6 = new Commande((long) 6, "Pétrole", c2);
	    Commande cm7 = new Commande((long) 7, "Tapis", c4);
	    Commande cm8 = new Commande((long) 8, "Silence", c1);
	    Commande cm9 = new Commande((long) 9, "Vélo", c1);
	    
	    CommandeRepository comrepo = (CommandeRepository) context.getBean(CommandeRepository.class);
	    comrepo.save(cm1);
	    comrepo.save(cm2);
	    comrepo.save(cm3);
	    comrepo.save(cm4);
	    comrepo.save(cm5);
	    comrepo.save(cm6);
	    comrepo.save(cm7);
	    comrepo.save(cm8);
	    comrepo.save(cm9);
	    
//		Une méthode permettant de trouver la liste des commandes associées à un produit
	    System.out.println("----------------------------------------------------------------------");
	    System.out.println("Une méthode permettant de trouver la liste des commandes associées à un produit");
	    System.out.println("----------------------------------------------------------------------");
	    System.out.println(comrepo.findByProduit("Vélo"));
	    
//		Une méthode permettant de trouver les commandes d’une personne.
	    System.out.println("----------------------------------------------------------------------");
	    System.out.println("Une méthode permettant de trouver les commandes d’une personne.");
	    System.out.println("----------------------------------------------------------------------");
	    System.out.println(comrepo.findByProprietaire(c5));
	    
//		Exercice2
	    System.out.println("----------------------------------------------------------------------");
	    System.out.println("Exercice2");
	    System.out.println("----------------------------------------------------------------------");
	    
//	    Ajout matière
	    System.out.println("----------------------------------------------------------------------");
	    System.out.println("Ajout matière");
	    System.out.println("----------------------------------------------------------------------");
	    
	    Matiere m1 = new Matiere((long) 1, "Programmation", 4);
	    Matiere m2 = new Matiere((long) 2, "Phisque", 3);
	    
	    MatiereRepository matRepo = (MatiereRepository) context.getBean(MatiereRepository.class);
	    matRepo.save(m1);
	    matRepo.save(m2);
	    
//	    Ajout etudiant
	    System.out.println("----------------------------------------------------------------------");
	    System.out.println("Ajout etudiant");
	    System.out.println("----------------------------------------------------------------------");
	    
	    Etudiant etu1 = new Etudiant((long) 1, "Maud", "Eric", new Date(1995, 06, 10));
	    Etudiant etu2 = new Etudiant((long) 2, "Fernandes", "Julien", new Date(1996, 06, 9));
	    Etudiant etu3 = new Etudiant((long) 3, "Massoussi", "Afef", new Date(1994, 07, 29));
	    
	    EtudiantRepository etuRepo = (EtudiantRepository) context.getBean(EtudiantRepository.class);
	    etuRepo.save(etu1);
	    etuRepo.save(etu2);
	    etuRepo.save(etu3);
	    
//	    Ajout evaluer
	    System.out.println("----------------------------------------------------------------------");
	    System.out.println("Ajout evaluer");
	    System.out.println("----------------------------------------------------------------------");
	
	    Evaluer eva1 = new Evaluer((long)1, new Date(2018, 04, 20), 17, new Etudiant((long) 1), new Matiere((long) 1));
	    Evaluer eva2 = new Evaluer((long)2, new Date(2018, 04, 20), 17, new Etudiant((long) 2), new Matiere((long) 1));
	    Evaluer eva3 = new Evaluer((long)3, new Date(2017, 04, 20), 18, new Etudiant((long) 1), new Matiere((long) 2));
	    EvaluerRepository evaRepo = (EvaluerRepository) context.getBean(EvaluerRepository.class);
	    evaRepo.save(eva1);
	    evaRepo.save(eva2);
	    evaRepo.save(eva3);
	    
//	    Une méthode permettant de trouver liste d’évaluation d’un étudiant.
	    System.out.println("----------------------------------------------------------------------");
	    System.out.println("Une méthode permettant de trouver liste d’évaluation d’un étudiant.");
	    System.out.println("----------------------------------------------------------------------");
	    System.out.println(evaRepo.findByEtudiant(etu1));
	    
//	    Une méthode permettant de trouver la liste des étudiants en fonction de la date de naissance.
	    System.out.println("----------------------------------------------------------------------");
	    System.out.println("Une méthode permettant de trouver la liste des étudiants en fonction de la date de naissance.");
	    System.out.println("----------------------------------------------------------------------");
	    System.out.println(etuRepo.findByDateN(new Date(1995, 06, 10)));
	    
//	    Une méthode permettant de trouver la liste des étudiants en fonction de la date de naissance.
	    System.out.println("----------------------------------------------------------------------");
	    System.out.println("Une méthode permettant de trouver la liste des étudiants en fonction de la date de naissance.");
	    System.out.println("----------------------------------------------------------------------");  
	    System.out.println(matRepo.findByCoefMat(4));
	    
//		Exercice4
	    System.out.println("----------------------------------------------------------------------");
	    System.out.println("Exercice4");
	    System.out.println("----------------------------------------------------------------------");
	    
	   
	    
	}

}
