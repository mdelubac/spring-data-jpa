package com.exercice1.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="COMMANDE")
public class Commande {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_COMMANDE")
	private Long idCommande;
	@Column(name="PRODUIT")
	private String produit;
	@ManyToOne 
	@JoinColumn(name="FK_CLIENT",referencedColumnName="ID_CLIENT")
	private Client proprietaire;
	
	public Commande() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Commande(String produit, Client proprietaire) {
		super();
		this.produit = produit;
		this.proprietaire = proprietaire;
	}

	public Commande(Long idCommande, String produit, Client proprietaire) {
		super();
		this.idCommande = idCommande;
		this.produit = produit;
		this.proprietaire = proprietaire;
	}

	public Long getIdCommande() {
		return idCommande;
	}

	public void setIdCommande(Long idCommande) {
		this.idCommande = idCommande;
	}

	public String getProduit() {
		return produit;
	}

	public void setProduit(String produit) {
		this.produit = produit;
	}

	public Client getProprietaire() {
		return proprietaire;
	}

	public void setProprietaire(Client proprietaire) {
		this.proprietaire = proprietaire;
	}

	@Override
	public String toString() {
		return "Commande [idCommande=" + idCommande + ", produit=" + produit + ", proprietaire=" + proprietaire + "]";
	}
	
	

}
