package com.exercice1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.exercice1.entity.Client;


@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
	//Une méthode permettant de trouver une liste des clients en fonction d’un nom. 
	public List<Client> findByNom(String nom);
	
	//Une méthode permettant de retourner la liste des clients dont l’âge est plus grand que le paramètre 
	public List<Client> findByAgeGreaterThan(Integer age);

	
}
