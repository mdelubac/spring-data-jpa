package com.exercice1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.exercice1.entity.Client;
import com.exercice1.entity.Commande;
@Repository
public interface CommandeRepository extends JpaRepository<Commande, Long> {
	//Une méthode permettant de trouver la liste des commandes associées à un produit. 
	public List<Commande> findByProduit(String produit);
	
	//Une méthode permettant de trouver les commandes d’une personne
	public List<Commande> findByProprietaire(Client client);
	

}
