package com.exercice2.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="ETUDIANT")
public class Etudiant implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="N_ETUDIANT")
	private Long nEtudiant;
	@Column(name="NOM")
	private String nom;
	@Column(name="PRENOM")
	private String prenom;
	@Column(name="DATE_N")
	private Date dateN;
	@OneToMany
	@JoinColumn(name="FK_ETUDIANT" ,referencedColumnName="N_ETUDIANT")
	private List<Evaluer> evaluers;
	
	public Etudiant() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public Etudiant(Long nEtudiant) {
		super();
		this.nEtudiant = nEtudiant;
	}


	public Etudiant(Long nEtudiant, String nom, String prenom, Date dateN) {
		super();
		this.nEtudiant = nEtudiant;
		this.nom = nom;
		this.prenom = prenom;
		this.dateN = dateN;
	}


	public Long getnEtudiant() {
		return nEtudiant;
	}

	public void setnEtudiant(Long nEtudiant) {
		this.nEtudiant = nEtudiant;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Date getDateN() {
		return dateN;
	}

	public void setDateN(Date dateN) {
		this.dateN = dateN;
	}

	public List<Evaluer> getEvaluers() {
		return evaluers;
	}

	public void setEvaluers(List<Evaluer> evaluers) {
		this.evaluers = evaluers;
	}

	@Override
	public String toString() {
		return "Etudiant [nEtudiant=" + nEtudiant + ", nom=" + nom + ", prenom=" + prenom + ", dateN=" + dateN
				+ "]";
	}
	
	
	
	
	
	

}
