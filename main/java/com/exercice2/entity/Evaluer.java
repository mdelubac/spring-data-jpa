package com.exercice2.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity
@Table(name="EVALUER")
public class Evaluer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID_EVALUER")
	private Long idEvaluer;
	@Column(name="DATE_E")
	private Date dateE;
	@Column(name="NOTE")
	private int note;
	@ManyToOne 
	@JoinColumn(name="FK_ETUDIANT" ,referencedColumnName="N_ETUDIANT")
	private Etudiant etudiant;
	@ManyToOne 
	@JoinColumn(name="FK_MATIERE" ,referencedColumnName="CODE_MAT")
	private Matiere matiere;
	
	public Evaluer() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Evaluer(Long idEvaluer, Date dateE, int note, Etudiant etudiant, Matiere matiere) {
		super();
		this.idEvaluer = idEvaluer;
		this.dateE = dateE;
		this.note = note;
		this.etudiant = etudiant;
		this.matiere = matiere;
	}

	public Long getIdEvaluer() {
		return idEvaluer;
	}

	public void setIdEvaluer(Long idEvaluer) {
		this.idEvaluer = idEvaluer;
	}

	public Date getDateE() {
		return dateE;
	}

	public void setDateE(Date dateE) {
		this.dateE = dateE;
	}

	public int getNote() {
		return note;
	}

	public void setNote(int note) {
		this.note = note;
	}

	public Etudiant getEtudiant() {
		return etudiant;
	}

	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}

	public Matiere getMatiere() {
		return matiere;
	}

	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}

	@Override
	public String toString() {
		return "Evaluer [idEvaluer=" + idEvaluer + ", dateE=" + dateE + ", note=" + note + ", etudiant=" + etudiant
				+ ", matiere=" + matiere + "]";
	}
	
	
	

}
