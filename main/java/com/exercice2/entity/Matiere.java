package com.exercice2.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="MATIERE")
public class Matiere implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="CODE_MAT")
	private Long codeMat;
	@Column(name="LIBEL_MAT")
	private String libelMat;
	@Column(name="COEF_MAT")
	private int coefMat;
	@OneToMany  
	@JoinColumn(name="FK_MATIERE" ,referencedColumnName="CODE_MAT")
	private List<Evaluer> evaluers;
	public Matiere() {
		
		super();
		// TODO Auto-generated constructor stub
	}

	
	
	public Matiere(Long codeMat) {
		super();
		this.codeMat = codeMat;
	}



	public Matiere(Long codeMat, String libelMat, int coefMat) {
		super();
		this.codeMat = codeMat;
		this.libelMat = libelMat;
		this.coefMat = coefMat;
	}


	public Long getCodeMat() {
		return codeMat;
	}
	public void setCodeMat(Long codeMat) {
		this.codeMat = codeMat;
	}
	public String getLibelMat() {
		return libelMat;
	}
	public void setLibelMat(String libelMat) {
		this.libelMat = libelMat;
	}
	public int getCoefMat() {
		return coefMat;
	}
	public void setCoefMat(int coefMat) {
		this.coefMat = coefMat;
	}
	public List<Evaluer> getEvaluers() {
		return evaluers;
	}
	public void setEvaluers(List<Evaluer> evaluers) {
		this.evaluers = evaluers;
	}
	@Override
	public String toString() {
		return "Matiere [codeMat=" + codeMat + ", libelMat=" + libelMat + ", coefMat=" + coefMat + "]";
	}
	
	

}
