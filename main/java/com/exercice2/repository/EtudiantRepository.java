package com.exercice2.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.exercice2.entity.Etudiant;
@Repository
public interface EtudiantRepository extends JpaRepository<Etudiant, Long>{
	//Une méthode permettant de trouver la liste des étudiants en fonction de la date de naissance
	public List<Etudiant> findByDateN (Date date);
	

}
