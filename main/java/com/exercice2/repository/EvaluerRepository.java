package com.exercice2.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.exercice2.entity.Etudiant;
import com.exercice2.entity.Evaluer;
@Repository
public interface EvaluerRepository extends JpaRepository<Evaluer, Long>{
	//Une méthode permettant de trouver liste d’évaluation d’un étudiant.
	public List<Evaluer> findByEtudiant(Etudiant etu);

}
