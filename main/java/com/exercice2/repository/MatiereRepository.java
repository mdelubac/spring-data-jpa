package com.exercice2.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.exercice2.entity.Matiere;
@Repository
public interface MatiereRepository extends JpaRepository<Matiere, Long>{
	//Une méthode permettant de trouver la liste des matières en fonction d'un coefficient donnée
	public List<Matiere> findByCoefMat(int coef);
	

}
