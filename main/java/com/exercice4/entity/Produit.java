package com.exercice4.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="PRODUIT")
public class Produit implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID_PRODUIT")
	private Long idProduit;
	@Column(name="REF_PRODUIT")
	private String refProduit;
	@Column(name="DESIGNATION")
	private String designation;
	@Column(name="PRIX")
	private int prix;
	@Column(name="QUANTITE")
	private int quantite;
	
	public Produit() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Produit(String refProduit, String designation, int prix, int quantite) {
		super();
		this.refProduit = refProduit;
		this.designation = designation;
		this.prix = prix;
		this.quantite = quantite;
	} 
	
	

}
