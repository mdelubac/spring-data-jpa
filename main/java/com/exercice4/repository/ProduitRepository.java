package com.exercice4.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.exercice4.entity.Produit;

public interface ProduitRepository extends JpaRepository<Produit, Long>{
/* Jpa repository nous propose directement les méthodes CRUD. Nous n'avons pas besoin d'écrire les méthodes pour:
	  -Ajouter un nouveau produit (save)
	  -Consulter tous les produits (findAll)
	  -Consulter un produit (findById)
	  -Supprimer un produit (delete)
*/
	
// Consulter les produits dont le nom contient un mot clé
	public List<Produit> findByRefProduit(String ref);
	
//- Consulter un produit à partir de sa désignation
	public Produit findByDesignation(String designation);


}
